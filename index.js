'use strict'
// eslint-disable-next-line
const tz = require('moment-timezone')
const moment = require('moment')
const TIME_ZONE = 'America/Lima'
const base = 'YYYY-MM-DD HH:MM:SS'

/**
*  Method to get now date
*  @param {String} format date format
*  @param {String} format date format
*/
const create = (date, format) => {
  let ndate = date || moment(new Date(), base).tz(TIME_ZONE)
  let nformat = format || base
  return format
    ? moment(ndate, base).tz(TIME_ZONE).format(nformat)
    : moment(ndate, nformat).tz(TIME_ZONE)
}

module.exports = {
  create
}

// Allow use of default import syntax in TypeScript
module.exports.default = create
