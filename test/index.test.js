const kdate = require('../index')

describe('Create', () => {
  test('No arguments ', () => {
    const date = kdate.create()
    const day = date.day()
    expect(day).toBeGreaterThan(0)
  })

  test('Just date ', () => {
    const date = kdate.create('01-12-2019')
    const month = date.month()
    expect(month).toBeGreaterThanOrEqual(0)
  })

  test('Just format ', () => {
    const year = kdate.create(null, 'YYYY')
    expect(year).toEqual('2019')
  })

  test('Date and format ', () => {
    const day = kdate.create('2019-01-25 ', 'dddd')
    expect(day).toBe('Friday')
  })
})
